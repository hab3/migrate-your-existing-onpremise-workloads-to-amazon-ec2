# Migrate Your Existing OnPremise Workloads to Amazon EC2
## Prerequisites 
> OnPremise VM (Preferably in VMWare / Virtualbox)
>
> If you have *.vmdk image of your VM that will also be enough
## Export VM & Upload to S3
> Create S3 bucket: **88uu88**
>
> Upload *.vmdk file on S3: bionic-server-cloudimg-amd64.vmdk
## Global Customization Variables
> Login to AWS Cloudshell with ‘Administrative’ rights by giving access keys and secret keys
>
> Go to AWS Cloudshell -> aws configure and give access keys and secret keys
>
![WoW](Screenshot2021-05-02at5.18.21AM.jpg "WoW")
>
> bucket_name="88uu88"
>
> vm_image_name="bionic-server-cloudimg-amd64.vmdk"
## Create a Role for migration 
### Step-1: Create Trust Policy
> Create the IAM trust policy json with the name **trust-policy.json**

      cat > "trust-policy.json" << "EOF"
      {
         "Version": "2012-10-17",
         "Statement": [
            {
               "Effect": "Allow",
               "Principal": { "Service": "vmie.amazonaws.com" },
               "Action": "sts:AssumeRole",
               "Condition": {
                  "StringEquals":{
                     "sts:Externalid": "vmimport"
                  }
               }
            }
         ]
      }
      EOF
      
### Step-2: Create the IAM Role for VM Import
> Ensure that you create the role with the name vmimport. Use the trust policy created in the previous step
>
      aws iam create-role --role-name vmimport --assume-role-policy-document "file://trust-policy.json"
### Step-3: Create the IAM Policy
> **role-policy.json** policy will be attached to the role vmimport created in the previous step. 
>
> The bucket name is picked up from the global variable

      echo '{
         "Version":"2012-10-17",
         "Statement":[
            {
               "Effect":"Allow",
               "Action":[
                  "s3:GetBucketLocation",
                  "s3:GetObject",
                  "s3:ListBucket" 
               ],
               "Resource":[
                  "arn:aws:s3:::'${bucket_name}'",
                  "arn:aws:s3:::'${bucket_name}'/*"
               ]
            },
            {
               "Effect":"Allow",
               "Action":[
                  "ec2:ModifySnapshotAttribute",
                  "ec2:CopySnapshot",
                  "ec2:RegisterImage",
                  "ec2:Describe*"
               ],
               "Resource":"*"
            }
         ]
      }
      ' | sudo tee role-policy.json
      
### Step-4: Attach policy to IAM Role
      aws iam put-role-policy --role-name vmimport \ --policy-name vmimport \ --policy-document file://role-policy.json
## Begin VM Image Import Task
> Set the metadata, 
>
      echo '[
      {
         "Description": "centosv7",
         "Format": "vmdk",
         "UserBucket": {
            "S3Bucket": "'${bucket_name}'",
            "S3Key": "'${vm_image_name}'"
         }
      }]
      ' > containers.json
## Begin VM Import
      aws ec2 import-image --description "centosv7" --disk-containers file://containers.json
      aws ec2 describe-import-image-tasks --import-task-ids "import-ami-0029f0305eab43416”
>
> After status is completed you can see the **AWS Images** and lauch EC2 from there
>
> Use username: **Ubuntu**
